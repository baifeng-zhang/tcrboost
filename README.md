## TCRboost for predicting cancer scores using high-throughput TCR repertoire sequencing data (for beta chain CDR3 regions)
## By Bo Li, bo.li@utsouthwestern.edu, Nov 1st, 2018, all rights reserved.

TCRboost is an R tool box which implements an ensemble tree classifier trained from machine learning method, to the CDR3 amino acid sequences to predict a cancer score, which is predictative of an immune repertoire being cancer-associated. As the classifers have been previously trained and saved, users may directly apply the classifers to their in-house TCR-seq data for cancer score estimations. 

## Recommanded analysis pipeline:
Please download all the files, including the TCRBoost R script, iSMART python script, data/ folder for TCRboost, and fasta file for iSMART, and put all the files into one directory.

Put all the TCR-seq data into another directory, for example /Users/TCR-seq/.

Required R packages: JOUBoost, pROC and seqinr

Open an R console, and source the TCRboost R script. This will load all the necessary R packages needed for calculation. Please make sure they are properly installed. 

The following commands will generate an R list object, named "CancerScoreData" for example:

	PrepareInputfiles("/Users/TCR-seq/")  ## This function works directly with AdaptiveBiotech outputs, and creates two folders under /Users/TCR-seq/: for_iSMART/ and iSMART_results/. It internally calls iSMART python script to cluster CDR3s into antigen-specific groups.

	CancerScoreData <- batchPrediction("/Users/TCR-seq/iSMART_results/") ## This function performs prediction of cancer scores for each file.

CancerScoreData contains 3 components:

CancerScoreData$S is the aggregated scores for all the samples, with variable name the name for each file.
CancerScoreData$FREQ is the clonal frequencies for each CDR3 sequence.
CancerScoreData$DL is the raw scores for each CDR3 sequence with lengths ranging from 12 to 16 amino acids. Remove samples with fewer than 5 CDR3 lengths:

	idx.remove <- which(sapply(CancerScoreData$DL, length)<5)

	FilteredScores <- CancerScoreData$S

	if(length(idx.remove)>0)FilteredScores <- CancerScoreData$S[-idx.remove]
